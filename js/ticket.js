$(document).ready(function(){
	load(1);
});

function load(page){
	var q= $("#q").val();
	$("#loader").fadeIn('slow');
	$.ajax({
		url:'./ajax/tickets.php?action=ajax&page='+page+'&q='+q,
		beforeSend: function(objeto){
			$('#loader').html('<img src="./images/ajax-loader.gif"> Cargando...');
		},
		success:function(data){
			$(".outer_div").html(data).fadeIn('slow');
			$('#loader').html('');
			
		}
	});
}

function prueba(){
	// valores constantes para los selectores, le coloque un id en la vista
	const category = document.querySelector('#category');
	const priority = document.querySelector('#priority');

	if (category.value == 3 ){ // Comprobacion que la categoria se produccion
		//priority.value = 1; // Asignacion de prioridad alta
		$('#priority').val(1);
		$('input[name="priority_id"]').val(1);
		$('#priority').prop('disabled', true); // Deshabilitar campo prioridad
		$('#input[name="priority_id"]').prop('disable', false); // Deshabilitar campo prioridad

	}
	else {
		priority.value = ""; // contenido vacio
		$('#priority').prop('disabled', false); // habilitar campo 
		$('input[name="priority_id"]').prop('disabled', true).val(''); // habilitar campo 
		
	}
}

function eliminar (id)
{
	var q= $("#q").val();
	if (confirm("Realmente deseas eliminar el ticket?")){	
		$.ajax({
			type: "GET",
			url: "./ajax/tickets.php",
			data: "id="+id,"q":q,
			beforeSend: function(objeto){
				$("#resultados").html("Mensaje: Cargando...");
			},
			success: function(datos){
				$("#resultados").html(datos);
				load(1);
			}
		});
	}
}
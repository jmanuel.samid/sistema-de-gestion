<?php
    $title ="Sobre Mi | ";
    include "head.php";
    include "sidebar.php";  
?>

    <div class="right_col" role="main"> <!-- page content -->
        <div class="">
            <div class="page-title">
            <br>
                <div class="row">
            	    <div class="col-md-6 col-md-offset-3">
                        <h1>Acerca del autor</h1>
                        <p>Hola, soy Jose Manuel Arias</a></strong>  creador de <a>Sistema de Gestion y Control de Incidencias</a>.</p>

                        <br><br>
                        <h1 style="text-align: center;">Sistema de Gestion y Control de Incidencias</h1>
                        <h3>Descripcion</h3>
                        <p> Este es un sistema para gestionar incidencias sobre  proyectos, ambiente, prioridades y mas, ideal para el banco de venezuela, para el futuro desarrollo y control de proyectos, gestion del personal, etc, el sistema esta desarrollado con PHP y MySQL.</p>
                        <p>Este sistema de gestion se puede agregar ticket, proyectos y ambiente, generar reportes y mucho mas.</p>

                        <h3>Dedicatoria</h3>
                        <p>Este sistema se lo dedico a mi madre por que desde que empece en el mundo de las computadoras ella me dio su apoyo emocional y economico, me compro computadoras, me pagaron mis estudios, estoy muy agradecido con ella, la amo.</p>

                        <h3>Modulos</h3>
                        <p>Defino a grandes rasgos los modulos generales del sistema</p>
                        <ul>
                        <li><b>Usuarios</b>: Se puede agregar editar y eliminar usuarios.</li>
                        <li><b>Tickets</b>: Cada usuario puede agregar un ticket.</li>
                        <li><b>Proyectos</b>: Cada usuario puede agregar los proyectos que desee.</li>
                        <li><b>Perfiles</b>: Los usuarios pueden rellenar su perfil</li>
                        <li><b>Ambiente</b>: Los usuarios agregar Ambientes.</li>
                        <li><b>Reportes</b>: Los usuarios pueden generar reportes.</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div><!-- /page content -->

<?php include "footer.php" ?>
